//
//  ViewController.swift
//  Table test project
//
//  Created by Evgeny Shishko on 02.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import UIKit

    class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var _table: UITableView!
    @IBOutlet weak var _button: UIButton!
    var items = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        _table.hidden = true
        
        self._table.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        // Инициализация содержания строк таблицы
        for i in 0...6 {
            items.append("Строка #\(i+1)")
        }
        items.append("Выход")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }

    @IBAction func buttonClick(sender: AnyObject) {
        let scaleX = self.view.frame.size
            .width / self._button.frame.size.width
        let scaleY = self.view.frame.size
            .height / self._button.frame.size.height
        
        // Проверка, на весь ли экран кнопка
        if scaleX > 1 {
        UIView.animateWithDuration(0.5, delay: 0, options: [],animations: {
            self._button.transform = CGAffineTransformMakeScale(scaleX,scaleY)
            }, completion: { finished in self._table.hidden = !self._table.hidden}
            )
        }
        else {
            UIView.animateWithDuration(0.5, animations: {
                self._table.hidden = !self._table.hidden
                self._button.transform = CGAffineTransformMakeScale(scaleX,scaleY)
            })
        }
    }
    
    // Возвращает кол-во элементов в таблице
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.items.count
        }
    
    // Задает значение ячейки соот-щее значение из массива items
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
           let cell:UITableViewCell = self._table.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
           cell.textLabel?.text = self.items[indexPath.row]
           return cell
        }
    
    // Происходит при нажатии на строчку
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (indexPath.row == 7){
            buttonClick(_table)} // Если 8 строка - сворачиваем таблицу
        }
        }



